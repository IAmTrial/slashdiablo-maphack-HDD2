#pragma once
#include <string>
#include <Windows.h>

namespace TrialLogger {
	void LogResults(POINT*);
	void LogResults(int);
	void LogResults(std::string);
}
