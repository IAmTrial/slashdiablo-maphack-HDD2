#include "TrialLogger.h"
#include <fstream>
#include <ios>

void TrialLogger::LogResults(std::string info) {
	std::ofstream myFile;
	myFile.open("TrialHackLog.log", std::ios::app);
	myFile << info << std::endl;
	myFile.close();
}

void TrialLogger::LogResults(int value) {
	TrialLogger::LogResults(std::to_string((_LONGLONG)value));
}

void TrialLogger::LogResults(POINT *info) {
	std::ofstream myFile;
	myFile.open("TrialHackLog.log", std::ios::app);
	myFile << "x: " << info->x << ", y: " << info->y << std::endl;
	myFile.close();
}
