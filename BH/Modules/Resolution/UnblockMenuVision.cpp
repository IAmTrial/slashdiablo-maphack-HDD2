#include "UnblockMenuVision.h"
#include "../../TrialLogger/TrialLogger.h"
#include "../../D2Ptrs.h"

void UnblockMenuVision::OnGameExit() {
	isInGame = false;
}

void UnblockMenuVision::OnGameJoin(const string& name, const string& pass, int diff) {
	isInGame = true;
}

void UnblockMenuVision::OnDraw() {
	if (!isInGame) {
		return;
	}

	if (*p_D2CLIENT_MenuBlockScreenMode == 3) {
		//*p_D2CLIENT_MenuBlockScreenMode = 0;
	}
}
