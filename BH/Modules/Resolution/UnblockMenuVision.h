#pragma once
#include "../Module.h"
class UnblockMenuVision : public Module
{
private:
	bool isInGame;
public:
	UnblockMenuVision() : Module("Unblock Menu Vision") {};
	~UnblockMenuVision() {};
	void OnDraw();
	void OnGameExit();
	void OnGameJoin(const string& name, const string& pass, int diff);
};

